// console.log('Hello world');


// Array methods
/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// Mutator Methods
// functions / methods that mutate or change an array
// These manipulates the original array performing task such as adding and removing elements.

console.log(`== Mutator Methods ==`);

console.log(`---------------------------------------------`);

let fruits = [`Apple`, `Orange`, `Kiwi`, `Dragon Fruit`];


// push()

/*
	- Adds an element in the end of an array AND returns the new array's length.
	- Syntax:
		arrayName.push[newElement];
*/

console.log(`=> push()`);

console.log(`Fruits array: `);
console.log(fruits);


// fruits[fruits.length] = "Mango";
let fruitsLength = fruits.push(`Mango`);
console.log(`Size/length of fruits array: ${fruitsLength}`);
console.log(`Mutated array from push('Mango'):`);
console.log(fruits);

fruits.push(`Avocado`, `Guava`); // action - already stored in a variable
// console.log(fruits.push(`Avocado`, `Guava`)); // returns new array's length
console.log(`Mutated Array from push ('Avocado', 'Guava')`);
console.log(fruits);


// using a function
/*
function addMultipleFruits(fruit1, fruit2, fruit3){
	fruits.push(fruit1, fruit2, fruit3);
	console.log(fruits);
}
addMultipleFruits(`Durian`, `Atis`, `Melon`);
*/


// pop()
/*
	- Removes the last element in an array AND returns the removed element
*/

console.log(`---------------------------------------------`);
console.log(`=> pop()`);

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log(`Mutated Array from pop method: `);
console.log(fruits);


// unshift

/*
	- Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log(`---------------------------------------------`);
console.log(`=> unshift()`);

fruits.unshift(`Lime`, `Banana`);
console.log(`Mutated Array from unshift method: ('Lime', 'Banana')`);
console.log(fruits);


// shift()

/*
	Removes an element at the beginning of an array AND returns the removed statement
*/

console.log(`---------------------------------------------`);
console.log(`=> shift()`);

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(`Mutated Array from shift method: `);
console.log(fruits);

// splice();

/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/


console.log(`---------------------------------------------`);
console.log(`=> splice()`);
		
// indices  - 0		1 		2
	// ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']
		// splice // delete count index  	// elements to be added

fruits.splice(1, 2, `Lime`, `Cherry`, `Tomato`); // the elements to be added are added through replacing from starting index
console.log(`Mutated Array from splice method: `);
console.log(fruits);


// we could also use splice to remove elements
// to do that our syntax should be:
/*
 - Syntax:
        arrayName.splice(startingIndex, deleteCount);

	fruits.splice(2,4, `Lime`, `Cherry`);
	console.log(fruits);
	fruits.splice(1,1);
*/


// sort();
/*
    -Rearranges the array elements in alphanumeric or numeric order
    -Syntax:
        - arrayName.sort();
*/

console.log(`---------------------------------------------`);
console.log(`=> sort()`);

fruits.sort();
console.log(`Mutated Array from sort method: `);
console.log(fruits);


// reverse();
/*
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/

console.log(`---------------------------------------------`);
console.log(`=> reverse()`);

fruits.reverse();
console.log(`Mutated Array from reverse method: `);
console.log(fruits);


// Non-mutator methods
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.
*/

console.log(`---------------------------------------------`);
console.log(`---------------------------------------------`);
console.log(`== Non-Mutator Methods ==`);
console.log(`---------------------------------------------`);

				// 0  	1 		2 	 3 		4 	 5 		6 	 7
let countries = [`US`, `PH`, `CAN`, `SG`, `TH`, `PH`, `FR`, `DE`];


// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

console.log(`=> indexOf()`);

let firstIndex = countries.indexOf(`PH`);
console.log(`Result of indexOf('PH') ${firstIndex}`);

							// element to add // starting index where to search
/*let firstIndex = countries.indexOf(`PH`, 2); // with a specified index to start
console.log(firstIndex);*/

let invalidCountry = countries.indexOf(`BR`);
console.log(`Result of indexOf('BR') ${invalidCountry}`);


// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/

console.log(`---------------------------------------------`);
console.log(`=> lastIndexOf()`);

let lastIndex = countries.lastIndexOf(`PH`);
console.log(`Result of lastIndex('PH') ${lastIndex}`);

let lastIndexStart = countries.lastIndexOf(`PH`, 4);
console.log(`Result of lastIndex('PH', 4) ${lastIndexStart}`)


// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/

console.log(`---------------------------------------------`);
console.log(`=> slice()`);

console.log(`Original countries array:`);
console.log(countries);

// Slicing off elements from specified index to the last element

	let sliceArrayA = countries.slice(2);
//	 0		1 	  2 	3  	   4	5	   6  	 7
// [`US`, `PH`, `CAN`, `SG`, `TH`, `PH`, `FR`, `DE`];
    console.log(`Result from slice(2):`);
    console.log(sliceArrayA);

// Slicing off element from specified index to another index. But the specified last index is not included in the return.

    let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
    console.log(`Result from slice(2, 5):`);
    console.log(sliceArrayB);

//	 -8		-7 	  -6 	-5 	  -4	-3	  -2  	 -1
// [`US`, `PH`, `CAN`, `SG`, `TH`, `PH`, `FR`, `DE`];
    let sliceArrayC = countries.slice(-3); // starts at the last array element
    console.log("Result from slice method:");
    console.log(sliceArrayC);


// toString

/*
	- Returns an array as a string, separated by commas.
	- Syntax:
		arrayName.toString();
*/

console.log(`---------------------------------------------`);
console.log(`=> toString()`);

// [`US`, `PH`, `CAN`, `SG`, `TH`, `PH`, `FR`, `DE`];
let stringArray = countries.toString();
console.log(`Result from toString method: `);
console.log(stringArray);
// console.log(stringArray[0]); // U
console.log(typeof stringArray); //to check if the array is converted to string.













